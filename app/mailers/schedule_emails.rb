class ScheduleEmails < ApplicationMailer
  def send_email(user, event)
    @user = user
    @event = event
    mail(to: @user.email, subject: @event.title + " is going to start in 15 minutes", body: @event.description)
  end
end

class ApplicationMailer < ActionMailer::Base
  default from: "healthylifecmpt276@gmail.com"
  layout 'mailer'
end

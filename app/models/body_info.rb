class BodyInfo < ActiveRecord::Base
    validates :gender, presence: true
    validates :age, presence: true
    validates :cm, presence: true, if: :no_feet_and_inches?
    validates :kg, presence:true, if: :no_pound?
    validates :pound, presence:true, if: :no_kg?
    validates :feet, presence:true, if: :no_cm_and_inches?

    
    def no_cm_and_inches?
        cm == nil && inches == nil
    end
    
    def no_feet_and_inches?
        feet == nil && inches == nil
    end
    
    def no_pound?
        pound == nil
    end
    
    def no_kg?
        kg == nil
    end
    
end

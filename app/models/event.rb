class Event < ActiveRecord::Base
  belongs_to :user
  
  validates :title, presence: true
  validates :description, presence: true
  validate :valid_dates

  def valid_dates
    if start_time >= end_time
      self.errors.add :end_time, ' has to be after start time'
    end
  end
  
end

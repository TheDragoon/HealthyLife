function calculateBMRmetric(age, gender, height, weight){

if (gender === "male"){
    var BMR = 66.5 + (13.75*weight) + (5.003*height) - (6.775*age);
}

if (gender === "female"){
    var BMR = 655.1 + (9.563*weight) + (1.850*height)-(4.676*age);
}

return BMR;
    
}

function calculateBMRimperial(age, gender, height, weight){
    if (gender === "male"){
    var BMR = 66 + (6.2*weight) + (12.7*height) - (6.76*age);
}

if (gender === "female"){
    var BMR = 655.1 + (4.35*weight) + (4.7*height) - (4.7*age);
}

return BMR;
}

function calculateMaintain(BMR, exercise_level){

if(exercise_level === "Little to no exercise")  {
    var maintain = BMR*1.2;
}  

if(exercise_level === "3 times a week"){
    var maintain = BMR*1.375;   
}
if(exercise_level === "4 times a week"){
    var maintain = BMR*1.55;    
}
if(exercise_level === "5 times a week"){
    var maintain = BMR*1.725;    
}
if(exercise_level === "Daily"){
    var maintain = BMR*1.9;
        
}

return maintain;
}

function loseOnePoundPerWeek(maintain){
    var calorieInTake = parseInt(maintain) - 500;

return calorieInTake;    
}

function loseTwoPoundPerWeek(maintain){
    var calorieInTake = parseInt(maintain) - 1000;
return calorieInTake;
}

function gainOnePoundPerMonth(maintain){
    var calorieInTake = parseInt(maintain) + 120;
return calorieInTake;
}

function gainTwoPoundPerMonth(maintain){
    var calorieInTake = parseInt(maintain) + 240;
return calorieInTake;
    
}

function calculate(){
    var age = document.getElementById("age").value;
    if (document.getElementById("gender1").checked){
         var gender = document.getElementById("gender1").value;
    }else{
         var gender = document.getElementById("gender2").value;
    }
    var feet = document.getElementById("feet").value;
    var inches = document.getElementById("inches").value;
    var cm = document.getElementById("cm").value;
    var pound = document.getElementById("pound").value;
    var kg = document.getElementById("kg").value;
    if (document.getElementById("exercise_level1").checked){
        var exercise_level = document.getElementById("exercise_level1").value;
    }else if (document.getElementById("exercise_level2").checked){
        var exercise_level = document.getElementById("exercise_level2").value;
    }else if (document.getElementById("exercise_level3").checked){
        var exercise_level = document.getElementById("exercise_level3").value;
    }else if (document.getElementById("exercise_level4").checked){
        var exercise_level = document.getElementById("exercise_level4").value;
    }else {
        var exercise_level = document.getElementById("exercise_level5").value;
    }
    if(age===""){
        alert("Please enter your age!");
    }else if(feet ==="" && inches!=""){
        alert("Please enter your height either in cm or feet and inches");
    }else if (cm ==="" && feet === "" && inches === ""){
        alert("Please enter your height either in cm or feet and inches");
    }else if (pound === "" && kg ===""){
        alert("Please enter your weight either in kg or pound!");
    }else if (age <= 0){
        alert("Your age can not be 0 or negative!")
    }else if (feet != "" && feet <=0){
        alert("Your height can not be 0 or negative!")
    }else if (inches != "" && inches <0){
        alert("Your height can not be 0 or negative!")
    }else if (cm != "" && cm <=0){
        alert("Your height can not be 0 or negative!")
    }else if (kg != "" && kg <= 0){
        alert("Your weight can not be 0 or negative!")
    }else if (pound != "" && pound <=0){
        alert("Your weight can not be 0 or negative!")
    }else{
    
    if (cm===""){
        if (pound === ""){
            pound = kg*(2.2);
        }
        if(inches === ""){
            inches = 0;
        }
        var BMR = calculateBMRimperial(parseInt(age), gender, (parseInt(feet*12) + parseInt(inches)), parseInt(pound));

    }else{
        if(kg === ""){
            kg = pound/(2.2);
        }
        var BMR = calculateBMRmetric(parseInt(age), gender, parseInt(cm), parseInt(kg));
    }
   
    var maintain = calculateMaintain(BMR, exercise_level);
    document.getElementById("Maintenance").innerHTML = "To maintain your current weight: You would need to consume " + parseInt(maintain) + " claories a day";
    document.getElementById("lose1lb").innerHTML = "To lose 1 pound (0.45kg) per week: You would need to consume " + loseOnePoundPerWeek(maintain) + "calories a day";
    document.getElementById("lose2lb").innerHTML = "To lose 2 pound (0.9kg) per week: You would need to consume "  + loseTwoPoundPerWeek(maintain) + "calories a day";
    document.getElementById("gain1lb").innerHTML = "To gain 1 pound (0.45kg) per month: You would need to consume "  + gainOnePoundPerMonth(maintain) + "calories a day"
    document.getElementById("gain2lb").innerHTML = "To gain 2 pound (0.9kg) per month: You would need to consume " + + gainTwoPoundPerMonth(maintain) + "calories a day" 
  
    return maintain;
    }    
  
 
    
}
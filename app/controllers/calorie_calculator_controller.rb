class CalorieCalculatorController < ApplicationController
     rescue_from ActiveRecord::RecordNotFound, :with => :record_not_found

  def record_not_found
    redirect_to caloriecalculator_catch_path
  
  end
  
  def index
   @user = current_user

  end
end

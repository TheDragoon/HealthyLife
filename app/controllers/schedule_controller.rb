class ScheduleController < ApplicationController
  def schedule
     @user = current_user
  end
  
  def home
  end
  
  def send_sms
    message = params[:message]
    number = params[:number]
    account_sid = 'AC48a125295c1174dd5ea12294c8ce691a'
    auth_token = '40761b9e7ecf954f4627b30d7b4f5d09'
    
    if (is_number(number) && number.length == 10)
      @client = Twilio::REST::Client.new account_sid, auth_token
      
      @message = @client.account.messages.create({:to => "+1"+"#{number}",
                                                  :from => "+16042656825",
                                                  :body => "#{message}"})
    end
    redirect_to schedule_path
  end
  
  def is_number string
    true if Integer(string) rescue false
  end
  
  def is_length string
    if string.length == 10
      return true
    else
      return false
    end
  end
  
end
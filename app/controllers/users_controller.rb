class UsersController < ApplicationController
  skip_before_action :verify_authenticity_token
  
  def admin
    @users = User.paginate(page: params[:page])
  end
  
  def destroy
    @user = User.find(params[:id])
    @user.destroy
    redirect_to :root
  end
  
  def edit
    @user = User.find(params[:id])
  end
  
  def update
    @user = User.find(params[:id])
      if @user.update(user_params)
        redirect_to :root
      else
        render :action => :edit
      end
  end

  
  def signup
    @user=User.new
  end
  

  
  def create
    @user = User.new(user_params)

    respond_to do |format|
      if @user.save
        session[:user_id] = @user.id
        format.html { redirect_to root_url, notice: 'User was successfully created.' }
      else
        format.html { render :signup }
      end
    end
  end

  def create_login_session
    user = User.find_by_email(params[:email])
    if user && user.authenticate(params[:password])
      session[:user_id] = user.id
      redirect_to :root
    else
      flash[:danger] = "Invalid email/password combination!"
      redirect_to :login
    end
  end

  def logout
    session[:user_id] = nil
    redirect_to :root
  end

  private
    def user_params
      params.require(:user).permit!
    end

end

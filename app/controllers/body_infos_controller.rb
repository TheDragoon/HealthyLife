class BodyInfosController < ApplicationController
  before_action :set_body_info, only: [:show, :edit, :update, :destroy]
   
 
  # GET /body_infos
  # GET /body_infos.json
  def index
    @body_infos = BodyInfo.all
  end

  # GET /body_infos/1
  # GET /body_infos/1.json
  def show
  end

  # GET /body_infos/new
  def new
    @body_info = BodyInfo.new
  end

  # GET /body_infos/1/edit
  def edit
  end

  # POST /body_infos
  # POST /body_infos.json
  def create
    @body_info = BodyInfo.new(body_info_params)

    respond_to do |format|
      if @body_info.save
        format.html { redirect_to @body_info, notice: 'Body info was successfully created.' }
        format.json { render :show, status: :created, location: @body_info }
      else
        format.html { render :new }
        format.json { render json: @body_info.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /body_infos/1
  # PATCH/PUT /body_infos/1.json
  def update
    respond_to do |format|
      if @body_info.update(body_info_params)
        format.html { redirect_to @body_info, notice: 'Body info was successfully updated.' }
        format.json { render :show, status: :ok, location: @body_info }
      else
        format.html { render :edit }
        format.json { render json: @body_info.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /body_infos/1
  # DELETE /body_infos/1.json
  def destroy
    @body_info.destroy
    respond_to do |format|
      format.html { redirect_to body_infos_url, notice: 'Body info was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_body_info
      @body_info = BodyInfo.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def body_info_params
      params.require(:body_info).permit(:gender, :cm, :feet, :inches, :age, :kg, :pound, :username)
    end
    
   
end

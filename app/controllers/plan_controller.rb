class PlanController < ApplicationController
  def confirm
    @plan = Plan.new(plan_params)

    respond_to do |format|
      if @plan.save
        format.html { redirect_to plan_url, notice: 'Plan was successfully created.' }
        format.json { render json: @plan, status: :created}
      else
        format.html { render :new }
        format.json { render json: @plan.errors, status: :unprocessable_entity }
      end
    end
  end
end
class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :null_session
  
  private
  def current_user
    begin
      @current_user ||= User.find(session[:user_id]) if session[:user_id]
    rescue
      session[:user_id] = nil
      return nil
    end
    rescue ActiveRecord::RecordNotFound
  
  end
  def this_body_info
    @this_body_info ||= BodyInfo.find(session[:user_id]) if session[:user_id]
  end
  
  def requires_logged_in
    if current_user == nil
      redirect_to root_url #Only happens if current_user is equal to nil
    end
  end
  
  helper_method :this_body_info
  helper_method :current_user
  


end

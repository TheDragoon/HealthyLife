json.array!(@body_infos) do |body_info|
  json.extract! body_info, :id, :gender, :cm, :feet, :inches, :age, :kg, :pound, :username
  json.url body_info_url(body_info, format: :json)
end

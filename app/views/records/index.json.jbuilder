json.array!(@records) do |record|
  json.extract! record, :id, :record_id, :time, :plan
  json.url record_url(record, format: :json)
end

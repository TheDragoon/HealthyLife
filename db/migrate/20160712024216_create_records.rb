class CreateRecords < ActiveRecord::Migration
  def change
    create_table :records do |t|
      t.integer :record_id
      t.integer :time
      t.string :plan

      t.timestamps null: false
    end
  end
end

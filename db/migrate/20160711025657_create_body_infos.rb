class CreateBodyInfos < ActiveRecord::Migration
  def change
    create_table :body_infos do |t|
      t.string :gender
      t.integer :cm
      t.integer :feet
      t.integer :inches
      t.integer :age
      t.integer :kg
      t.integer :pound
      t.string :username

      t.timestamps null: false
    end
  end
end

require 'test_helper'

class BodyInfosControllerTest < ActionController::TestCase
  setup do
    @body_info = body_infos(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:body_infos)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create body_info" do
    assert_difference('BodyInfo.count') do
      post :create, body_info: { age: @body_info.age, cm: @body_info.cm, feet: @body_info.feet, gender: @body_info.gender, inches: @body_info.inches, kg: @body_info.kg, pound: @body_info.pound, username: @body_info.username }
    end

    assert_redirected_to body_info_path(assigns(:body_info))
  end

  test "should show body_info" do
    get :show, id: @body_info
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @body_info
    assert_response :success
  end

  test "should update body_info" do
    patch :update, id: @body_info, body_info: { age: @body_info.age, cm: @body_info.cm, feet: @body_info.feet, gender: @body_info.gender, inches: @body_info.inches, kg: @body_info.kg, pound: @body_info.pound, username: @body_info.username }
    assert_redirected_to body_info_path(assigns(:body_info))
  end

  test "should destroy body_info" do
    assert_difference('BodyInfo.count', -1) do
      delete :destroy, id: @body_info
    end

    assert_redirected_to body_infos_path
  end
end

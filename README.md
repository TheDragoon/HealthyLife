CMPT 276 Project Healthy Life - Group 6

To run project:
1. Pull project from git
2. Run rake db:migrate to migrate all the databases
3. Run rake db:seed for admin account
4. Run rake jobs:work to start worker

Rails.application.routes.draw do
  
  get "/events/calendar" => "events#calendar"
  resources :events
  resources :records
  resources :body_infos
  get 'calorie_calculator/index'

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".
  resources :users
  resources :calorie_calculator
  # You can have the root of your site routed with "root"
  root 'page#home'
  
  get '/welcome' => 'page#welcome'
  
  get '/about' => 'page#about'
  
  get 'signup' => 'users#signup'
  
  get "login" => "users#login", :as => "login"
  post "create_login_session" => "users#create_login_session"
  
  delete "logout" => "users#logout", :as => "logout"
  
  get 'admin' => 'users#admin'
  
  get 'delete' => 'students#destroy'
  
  get 'Plan', to: 'plan#plan', as: 'plan'
  
  get 'Schedule', to: 'schedule#schedule', as: 'schedule'
     
  get 'Calorie Calculator', to: 'calorie_calculator#index', as: 'caloriecalculator'
  
  get 'Calorie Calculator Catch', to: 'calorie_calculator#catch', as: 'caloriecalculator_catch'
  
  post '/send_sms' => 'schedule#send_sms'
  
  get 'auth/facebook'
  get 'auth/facebook/callback', to: 'sessions#create'
  
end
